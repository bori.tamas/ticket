package hu.mycompany.ticket.ticket;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import hu.mycompany.ticket.api.exception.GlobalExceptionHandler;

@RestControllerAdvice
public class TicketExceptionHandler extends GlobalExceptionHandler {

}
