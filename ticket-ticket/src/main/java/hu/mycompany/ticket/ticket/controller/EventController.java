package hu.mycompany.ticket.ticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.mycompany.ticket.api.dto.EventSeatsDto;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.exception.EventNotFoundException;
import hu.mycompany.ticket.api.handlers.ReserveHandlerIF;
import hu.mycompany.ticket.api.handlers.TicketHandlerIF;
import hu.mycompany.ticket.ticket.service.EventService;

@RestController
@RequestMapping("/events")
public class EventController implements TicketHandlerIF, ReserveHandlerIF {
    
    @Autowired
    private EventService eventService;
    
    @Override
    @GetMapping
    public EventsDto getEvents() {
        return eventService.getEvents();
    }

    @Override
    @GetMapping("/{eventId}")
    public EventSeatsDto getEventById(@RequestParam int eventId) throws EventNotFoundException {
        return eventService.getEventById(eventId);
    }

    @Override
    @PostMapping
    public ReserveDto reserve(@RequestParam int eventId, @RequestParam int userId, @RequestParam String cardNumber, @RequestParam String seatNumber) {
        return eventService.reserve(eventId, userId, cardNumber, seatNumber);
    }
}
