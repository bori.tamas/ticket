package hu.mycompany.ticket.ticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.mycompany.ticket.api.domain.Seat;
import hu.mycompany.ticket.api.dto.EventSeatsDto;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.exception.EventNotFoundException;
import hu.mycompany.ticket.api.exception.SeatReservedException;
import hu.mycompany.ticket.api.handlers.PartnerHandlerIF;
import hu.mycompany.ticket.api.handlers.PaymentHandlerIF;

@Service
public class EventService {

    @Autowired
    private PartnerHandlerIF partnerHandlerIF;

    @Autowired
    private PaymentHandlerIF paymentHandlerIF;

    public EventsDto getEvents() {
        EventsDto events = partnerHandlerIF.getEvents();

        return events;
    }

    public EventSeatsDto getEventById(int eventId)  {
        EventSeatsDto eventSeats = new EventSeatsDto();
        try {
                eventSeats = partnerHandlerIF.getEventById(eventId);
            } catch (Exception enfe) {
                throw new EventNotFoundException("Nincs ilyen esemény!");
        }
        return eventSeats;
    }

    public synchronized ReserveDto reserve(int eventId, int userId, String cardNumber, String seatNumber) {
        EventSeatsDto event = getEventById(eventId);
        for (Seat seat : event.getData().getSeats()) {
            if (seat.getId().equals(seatNumber)) {
                if (seat.getReserved().toLowerCase().equals("true")) {
                    throw new SeatReservedException("Már lefoglalt székre nem lehet jegyet eladni!");
                } else {
                    ReserveDto paymentReserve = paymentHandlerIF.pay(eventId, userId, cardNumber, seat.getPrice());
                    if (paymentReserve != null && paymentReserve.getSuccess().equals("true")) {
                        return partnerHandlerIF.reserve(eventId, seat.getId());
                    }
                }
            }
        }
        throw new SeatReservedException("");
    }
}