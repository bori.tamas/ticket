package hu.mycompany.ticket.api.enums;

public enum ErrorCodes {
    EVENT_NOT_FOUND("90001"),
    SEAT_NOT_FOUND("90002"),
    SEAT_NOT_AVALABLE("90010"),
    TOKEN_ERROR("10050"),
    TOKEN_EXPIRED_ERROR("10051"),
    BANKCARD_ERROR("10100"),
    INSUFFICIENT_MONEY_ERROR("10101"),
    DEFAULT_ERROR("400");

    private final String code;

    ErrorCodes(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
