package hu.mycompany.ticket.api.domain;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Entity to represent general BL logging details.
 */
public class BusinessLog {

    private UUID id;

    private String logType;

    private String logSubType;

    private String logValue;

    private LocalDateTime timestamp;

    public UUID getId() {
        return id;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getLogSubType() {
        return logSubType;
    }

    public void setLogSubType(String logSubType) {
        this.logSubType = logSubType;
    }

    public String getLogValue() {
        return logValue;
    }

    public void setLogValue(String logValue) {
        this.logValue = logValue;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Constructor.
     */
    public BusinessLog(BusinessLogType logType, String logSubType, String logValue) {
        this.id = UUID.randomUUID();
        this.timestamp = LocalDateTime.now();
        this.logType = logType.toString();
        this.logSubType = logSubType;
        this.logValue = logValue;
    }
}
