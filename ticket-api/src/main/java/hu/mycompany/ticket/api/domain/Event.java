package hu.mycompany.ticket.api.domain;

public class Event {
    
    private int eventId;
    private String title;
    private String location;
    private int startTimeStamp;
    private int endTimeStamp;

    public int getEventId() {
        return eventId;
    }
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public int getStartTimeStamp() {
        return startTimeStamp;
    }
    public void setStartTimeStamp(int startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }
    public int getEndTimeStamp() {
        return endTimeStamp;
    }
    public void setEndTimeStamp(int endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }
}
