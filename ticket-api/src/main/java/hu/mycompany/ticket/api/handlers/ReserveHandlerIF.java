package hu.mycompany.ticket.api.handlers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.mycompany.ticket.api.dto.ReserveDto;

public interface ReserveHandlerIF {

    @PostMapping("/{eventId}/{userId}/{cardNumber}/{seatNumber}")
    public ReserveDto reserve(@RequestParam int eventId, @RequestParam int userId, @RequestParam String cardNumber, @RequestParam String seatNumber) throws Exception;
}
