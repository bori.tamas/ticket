package hu.mycompany.ticket.api.exception;

public class InvalidTokenException extends RuntimeException {

    private static final long serialVersionUID = -7810059649641278587L;

    /**
     * Exception to be thrown when the given token is invalid.
     */
    public InvalidTokenException(String message) {
        super(message);
    }
}