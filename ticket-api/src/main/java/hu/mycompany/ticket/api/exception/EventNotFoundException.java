package hu.mycompany.ticket.api.exception;

public class EventNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -1538333413748381646L;

    /**
     * Exception to be thrown when the given event is not found in the connected entities.
     */
    public EventNotFoundException(String message) {
        super(message);
    }
}
