package hu.mycompany.ticket.api.domain;

import java.util.List;

public class EventSeat {
    
    private int eventId;
    private List<Seat> seats;
    public int getEventId() {
        return eventId;
    }
    public void setEventId(int eventId) {
        this.eventId = eventId;
    }
    public List<Seat> getSeats() {
        return seats;
    }
    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
}
