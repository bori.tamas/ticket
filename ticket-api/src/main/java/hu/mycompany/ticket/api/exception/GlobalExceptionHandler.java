package hu.mycompany.ticket.api.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import hu.mycompany.ticket.api.dto.ErrorResponseDto;
import hu.mycompany.ticket.api.enums.ErrorCodes;

import org.springframework.http.HttpStatus;

/**
 * The Global Exception Handler for the ticket application api module.
 */
@RestControllerAdvice
public abstract class GlobalExceptionHandler {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    private static final String DEFAULT_ERROR_MSG = "Problem while processing the request!";

    /**
     * Helper method, for handling the default (Exception) exception.
     *
     * @param ex - The exception, that holds the message that we want to pass to the client.
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDto handleDefaultException(Exception ex) {

        LOGGER.error("Handling Exception type of exception! -> ", ex);
        return new ErrorResponseDto(ErrorCodes.DEFAULT_ERROR.getCode(), DEFAULT_ERROR_MSG);
    }

    /**
     * Helper method, for handling the SeatReservedException type of exception.
     *
     * @param ex - The thrown runtime exception, that needs to be handled.
     */
    @ExceptionHandler(SeatReservedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseDto handleSeatReservedException(Exception ex) {

        LOGGER.error("Handling SeatReservedException type of exception! -> {}",
            ex.getMessage());
        return new ErrorResponseDto(ErrorCodes.SEAT_NOT_AVALABLE.getCode(), ex.getMessage());
    }
    
    /**
     * Helper method, for handling the SeatNotFoundException type of exception.
     *
     * @param ex - The thrown runtime exception, that needs to be handled.
     */
    @ExceptionHandler(SeatNotFoundException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseDto handleSeatNotFoundException(Exception ex) {

        LOGGER.error("Handling SeatNotFoundException type of exception! -> {}",
            ex.getMessage());
        return new ErrorResponseDto(ErrorCodes.SEAT_NOT_FOUND.getCode(), ex.getMessage());
    }
    
    /**
     * Helper method, for handling the EventNotFoundException type of exception.
     *
     * @param ex - The thrown runtime exception, that needs to be handled.
     */
    @ExceptionHandler(EventNotFoundException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseDto handleEventNotFoundException(Exception ex) {

        LOGGER.error("Handling EventNotFoundException type of exception! -> {}",
            ex.getMessage());
        return new ErrorResponseDto(ErrorCodes.EVENT_NOT_FOUND.getCode(), ex.getMessage());
    }
    
    /**
     * Helper method, for handling the InsufficientMoneyException type of exception.
     *
     * @param ex - The thrown runtime exception, that needs to be handled.
     */
    @ExceptionHandler(InsufficientMoneyException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseDto handleInsufficientMoneyExceptionn(Exception ex) {

        LOGGER.error("Handling InsufficientMoneyException type of exception! -> {}",
            ex.getMessage());
        return new ErrorResponseDto(ErrorCodes.INSUFFICIENT_MONEY_ERROR.getCode(), ex.getMessage());
    }
    
    /**
     * Helper method, for handling the UserBankCardException type of exception.
     *
     * @param ex - The thrown runtime exception, that needs to be handled.
     */
    @ExceptionHandler(UserBankCardException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorResponseDto handleUserBankCardException(Exception ex) {

        LOGGER.error("Handling UserBankCardException type of exception! -> {}",
            ex.getMessage());
        return new ErrorResponseDto(ErrorCodes.BANKCARD_ERROR.getCode(), ex.getMessage());
    }
    
    /**
     * Helper method, for handling the InvalidTokenException type of exception.
     *
     * @param ex - The thrown runtime exception, that needs to be handled.
     */
    @ExceptionHandler(InvalidTokenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorResponseDto handleInvalidTokenException(Exception ex) {

        LOGGER.error("Handling UserBankCardException type of exception! -> {}",
            ex.getMessage());
        return new ErrorResponseDto(ErrorCodes.TOKEN_ERROR.getCode(), ex.getMessage());
    }
}