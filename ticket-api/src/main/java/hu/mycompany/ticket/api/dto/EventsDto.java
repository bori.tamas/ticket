package hu.mycompany.ticket.api.dto;

import java.util.List;

import hu.mycompany.ticket.api.domain.Event;

public class EventsDto implements Response {

    private static final long serialVersionUID = 4393655082018437920L;
    
    private List<Event> data;
    
    private String success;

    public List<Event> getData() {
        return data;
    }

    public void setData(List<Event> data) {
        this.data = data;
    }

    public String isSuccess() {
        return success;
    }

    public void setSuccess(String succes) {
        this.success = succes;
    }
}
