package hu.mycompany.ticket.api.exception;

public class UserBankCardException extends RuntimeException {

    private static final long serialVersionUID = 6599975407050542881L;

    /**
     * Exception to be thrown when the given user bank card doesn't belong to the given user.
     */
    public UserBankCardException(String message) {
        super(message);
    }
}
