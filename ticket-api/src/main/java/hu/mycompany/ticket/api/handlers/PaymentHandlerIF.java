package hu.mycompany.ticket.api.handlers;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.mycompany.ticket.api.dto.ReserveDto;

@FeignClient(name="PaymentClient", url= "http://localhost:8081/core/payment")
public interface PaymentHandlerIF {

    @PostMapping("/{eventId}/{userId}/{cardNumber}/{seatPrice}")
    public ReserveDto pay(@RequestParam("eventId") int eventId, @RequestParam("userId") int userId, 
            @RequestParam("cardNumber") String cardNumber, @RequestParam("seatPrice") int seatPrice);
}