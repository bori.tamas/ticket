package hu.mycompany.ticket.api.dto;

import hu.mycompany.ticket.api.domain.EventSeat;

public class EventSeatsDto implements Response {

    private static final long serialVersionUID = -7130646432771198331L;

    private EventSeat data;
    
    private String success;

    public EventSeat getData() {
        return data;
    }

    public void setData(EventSeat data) {
        this.data = data;
    }

    public String isSuccess() {
        return success;
    }

    public void setSuccess(String succes) {
        this.success = succes;
    }
}
