package hu.mycompany.ticket.api.exception;

public class SeatNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 517301778083066153L;

    /**
     * Exception to be thrown when the given seat is not found in the connected entities.
     */
    public SeatNotFoundException(String message) {
        super(message);
    }
}
