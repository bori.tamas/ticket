package hu.mycompany.ticket.api.dto;

public class ReserveDto implements Response {

    private static final long serialVersionUID = 7063284903125083621L;

    private String reservationId;

    private String success;

    public String getReservationId() {
        return reservationId;
    }
    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }
    public String getSuccess() {
        return success;
    }
    public void setSuccess(String success) {
        this.success = success;
    } 
}