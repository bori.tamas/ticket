package hu.mycompany.ticket.api.exception;

public class InsufficientMoneyException extends RuntimeException {

    private static final long serialVersionUID = -9028597194939140085L;

    /**
     * Exception to be thrown when the given user doesn't have enough money for the selected seat. 
     */
    public InsufficientMoneyException(String message) {
        super(message);
    }
}
