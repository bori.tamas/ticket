package hu.mycompany.ticket.api.exception;

public class SeatReservedException extends RuntimeException {

    private static final long serialVersionUID = -5780933508372243519L;

    /**
     * Exception to be thrown when the given seat is reserved.
     */
    public SeatReservedException(String message) {
        super(message);
    }
}