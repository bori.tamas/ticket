package hu.mycompany.ticket.api.handlers;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.mycompany.ticket.api.dto.EventSeatsDto;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.exception.EventNotFoundException;

@FeignClient(name="PartnerClient", url= "http://localhost:8082/partner/partner")
public interface PartnerHandlerIF {

    @GetMapping
    public EventsDto getEvents();

    @GetMapping("/{eventId}")
    public EventSeatsDto getEventById(@RequestParam("eventId") int eventId) throws EventNotFoundException;
    
    @PostMapping("/{eventId}/{seatId}")
    public ReserveDto reserve(@RequestParam("eventId") int eventId, @RequestParam("seatId") String seatId);
}