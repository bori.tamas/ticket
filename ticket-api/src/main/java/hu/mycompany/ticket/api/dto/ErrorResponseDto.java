package hu.mycompany.ticket.api.dto;

import java.util.Collections;
import java.util.List;

public class ErrorResponseDto {

    private String errorCode;
    private String errorMessage;
    private List<String> details;

    /**
     * Default constructor.
     */
    public ErrorResponseDto() {
    }

    /**
     * The constructor for implementing the ErrorResponseDto with the passed values.
     *
     * @param status       The HttpStatus integer value.
     * @param errorMessage The Error message that we want to pass to the client.
     */
    public ErrorResponseDto(String status, String errorMessage) {
        this.errorCode = status;
        this.errorMessage = errorMessage;
        this.details = Collections.emptyList();
    }

    /**
     * Constructor to create an ErrorResponseDto with the passed values and additional details.
     *
     * @param status       The HttpStatus integer value.
     * @param errorMessage The Error message that we want to pass to the client.
     * @param details      The details of the error.
     */
    public ErrorResponseDto(String status, String errorMessage, List<String> details) {
        this.errorCode = status;
        this.errorMessage = errorMessage;
        this.details = details;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }
}
