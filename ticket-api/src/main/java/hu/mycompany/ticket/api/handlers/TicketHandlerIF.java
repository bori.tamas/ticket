package hu.mycompany.ticket.api.handlers;

import org.springframework.web.bind.annotation.GetMapping;

import hu.mycompany.ticket.api.dto.EventSeatsDto;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.api.exception.EventNotFoundException;

public interface TicketHandlerIF {

    @GetMapping
    public EventsDto getEvents();
    
    @GetMapping("/{id}")
    public EventSeatsDto getEventById(int eventId) throws EventNotFoundException;
}
