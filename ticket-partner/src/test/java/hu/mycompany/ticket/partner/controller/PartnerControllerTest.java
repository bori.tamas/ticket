package hu.mycompany.ticket.partner.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import hu.mycompany.ticket.api.domain.Event;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.partner.service.PartnerService;

/**
 * The RestController Test class for PartnerController endpoints.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = PartnerController.class, secure = false)
public class PartnerControllerTest {

    private static final String ENDPOINT = "/partner";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PartnerService partnerService;

    private EventsDto events;
    
    private Event event;

    /**
     * The method where we initialize and set up the objects needed for the tests.
     */
    @Before
    public void init() {
        events = new EventsDto();
        List<Event> eventList = new ArrayList<>();
        event = new Event();
        event.setEventId(1000);
        event.setLocation("Nowhere!");
        event.setTitle("Esemény1");
        event.setStartTimeStamp(1212);
        event.setEndTimeStamp(999);
        events.setData(eventList);
        events.setSuccess("true");
    }

    @Test
    public void getEventsTest() throws Exception {
        when(partnerService.getEvents()).thenReturn(events);

        mvc.perform(get(ENDPOINT).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(partnerService, times(1)).getEvents();
    }
}
