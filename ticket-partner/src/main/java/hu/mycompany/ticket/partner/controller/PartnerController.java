package hu.mycompany.ticket.partner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hu.mycompany.ticket.api.dto.EventSeatsDto;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.handlers.PartnerHandlerIF;
import hu.mycompany.ticket.partner.service.PartnerService;

@RestController
@RequestMapping("/partner")
public class PartnerController implements PartnerHandlerIF {

    @Autowired
    private PartnerService partnerService;

    @Override
    @GetMapping
    public EventsDto getEvents() {
        
        return partnerService.getEvents();
    }

    @Override
    @GetMapping("/{eventId}")
    public EventSeatsDto getEventById(@RequestParam int eventId) {
        
        return partnerService.getEventById(eventId);
    }

    @Override
    public ReserveDto reserve(int eventId, String seatId) {
        return partnerService.reserve(eventId, seatId);
    }
}