package hu.mycompany.ticket.partner.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import hu.mycompany.ticket.api.dto.EventSeatsDto;
import hu.mycompany.ticket.api.dto.EventsDto;
import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.exception.EventNotFoundException;
import hu.mycompany.ticket.api.exception.SeatReservedException;

@Service
public class PartnerService {

    public EventsDto getEvents() {
        EventsDto events = new EventsDto();
        Path path = Paths.get("files/getEvents.json");

        try {
            String result = Files.readString(path,
                    Charset.defaultCharset());
            events = new Gson().fromJson(result, EventsDto.class);

        } catch (IOException e) {
            throw new EventNotFoundException("Események nem találhatóak!");
        }
        return events;
    }
    
    public EventSeatsDto getEventById(int eventId) throws EventNotFoundException {
        EventSeatsDto eventSeats = new EventSeatsDto();
        Path path = Paths.get("files/getEvent" + eventId + ".json");

        try {
            String result = Files.readString(path,
                    Charset.defaultCharset());
            eventSeats = new Gson().fromJson(result, EventSeatsDto.class);
        } catch (IOException e) {
            throw new EventNotFoundException("Nincs ilyen esemény!");
        }
        return eventSeats;
    }
    
    public ReserveDto reserve(int eventId, String seatId) {
        EventSeatsDto eventSeats = new EventSeatsDto();
        Path path = Paths.get("files/getEvent" + eventId + ".json");

        try {
            String result = Files.readString(path,
                    Charset.defaultCharset());
            eventSeats = new Gson().fromJson(result, EventSeatsDto.class);
            eventSeats.getData().getSeats().forEach((seat) -> {
                if (seat.getId().equals(seatId)) {
                    if (seat.getReserved().toLowerCase().equals("true")) {
                        throw new SeatReservedException("Már lefoglalt székre nem lehet jegyet eladni!");
                    } else {
                        seat.setReserved("true");
                    }
                }
            });
            String newSeat = new Gson().toJson(eventSeats, EventSeatsDto.class);
            Files.writeString(path, newSeat, Charset.defaultCharset());
            ReserveDto reserve  = new ReserveDto();
            Random random = new Random();
            reserve.setReservationId(String.valueOf(random.nextInt(10)));
            reserve.setSuccess("true");
            return reserve;
        } catch (IOException e) {
            throw new EventNotFoundException("Nincs ilyen esemény!");
        }
    }
}