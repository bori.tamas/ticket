package hu.mycompany.ticket.partner.config;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import hu.mycompany.ticket.api.exception.GlobalExceptionHandler;

@RestControllerAdvice
public class PartnerExceptionHandler extends GlobalExceptionHandler {

}
