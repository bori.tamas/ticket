FROM adoptopenjdk/openjdk11
VOLUME /tmp
ADD /ticket-partner/target/ticket-partner-0.0.1-SNAPSHOT.jar appP.jar
ADD /ticket-core/target/ticket-core-0.0.1-SNAPSHOT.jar appC.jar
ADD /ticket-ticket/target/ticket-ticket-0.0.1-SNAPSHOT.jar appT.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
RUN sh -c 'touch /app.jar'
