package hu.mycompany.ticket.core.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.core.service.PaymentService;

/**
 * The RestController Test class for AdvertisementController endpoints.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = PaymentController.class, secure = false)
public class PaymentControllerTest {

    private static final String ENDPOINT = "/payment";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PaymentService paymentService;

    private ReserveDto reserve;

    /**
     * The method where we initialize and set up the objects needed for the tests.
     */
    @Before
    public void init() {
        reserve = new ReserveDto();
        reserve.setReservationId("112121212");
        reserve.setSuccess("true");
    }

    @Test
    @Ignore
    public void payTest() throws Exception {
        when(paymentService.pay(2, 2000, "1212121", 200)).thenReturn(reserve);

        mvc.perform(post(ENDPOINT).contentType(MediaType.APPLICATION_JSON).requestAttr("eventId", 2).requestAttr("userId", 2000)
                .requestAttr("cardNumber", "1212121").requestAttr("seatPrice", 200))
                .andExpect(status().isOk());

        verify(paymentService, times(1)).pay(2, 2000, "1212121", 200);
    }
}