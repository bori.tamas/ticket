package hu.mycompany.ticket.core.service;

import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.mycompany.ticket.core.domain.UserDevice;
import hu.mycompany.ticket.core.domain.UserToken;
import hu.mycompany.ticket.core.domain.Users;
import hu.mycompany.ticket.core.repository.UserDeviceRepository;
import hu.mycompany.ticket.core.repository.UserTokenRepository;
import hu.mycompany.ticket.core.repository.UsersRepository;

@Service
public class AuthenticationService {

    @Autowired
    private UsersRepository usersRepository;
    
    @Autowired
    private UserDeviceRepository userDeviceRepository;
    
    @Autowired
    private UserTokenRepository userTokenRepository;
    
    /**
     * Checks if the token is valid.
     */
    @Transactional
    public boolean checkToken(int userId, String token) {
        Users user = usersRepository.findByUserId(userId);
        List<UserDevice> devices = userDeviceRepository.findByUserId(userId);
        String rawString = user.getEmail()+"&"+userId+"&";
        boolean hasFound = false;
        for (UserDevice device: devices) {
            String utf8EncodedString = Base64.getEncoder().encodeToString(rawString.concat(device.getDeviceHash()).getBytes());
            if (utf8EncodedString.equals(token)) {
                hasFound = true;
                UserToken saveToken = new UserToken();
                saveToken.setUserId(userId);
                saveToken.setToken(token);
                userTokenRepository.save(saveToken);
            }
        }
        
        return hasFound;
    }
}
