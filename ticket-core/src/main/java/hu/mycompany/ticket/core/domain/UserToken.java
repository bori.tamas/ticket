package hu.mycompany.ticket.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Entity to represent one record from the collection of user token in a separate
 * table.
 */
@Entity
@Audited
@EntityListeners(AuditingEntityListener.class)
@Table(name = "usertoken")
public class UserToken {

    @Id
    @Column(name = "usertokenid", updatable = false)
    private int userTokenId;
    
    @Column(name = "userid", updatable = false)
    private int userId;
    
    @Column(name = "token", updatable = false)
    private String token;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserTokenId() {
        return userTokenId;
    }

    public void setUserTokenId(int userTokenId) {
        this.userTokenId = userTokenId;
    }
}
