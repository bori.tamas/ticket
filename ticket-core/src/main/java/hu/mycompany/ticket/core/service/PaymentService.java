package hu.mycompany.ticket.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.exception.SeatReservedException;
import hu.mycompany.ticket.api.exception.UserBankCardException;
import hu.mycompany.ticket.core.domain.UserBankCard;
import hu.mycompany.ticket.core.repository.UserBankCardRepository;

@Service
public class PaymentService {

    @Autowired
    private AuthenticationService authService;

    @Autowired
    private UserBankCardRepository paymentRepository;

    /**
     * Do the pay.
     */
    @Transactional
    public ReserveDto pay(int eventId, int userId, String cardNumber, int seatPrice) {

        authService.checkToken(userId, "");
        UserBankCard card = paymentRepository.findByUserId(userId);
        if (card != null && !card.getCardnumber().equals(cardNumber)) {
            throw new UserBankCardException("Bankcard doesn't belong to this user!");
        }
        if (card.getAmount() < seatPrice) {
            throw new SeatReservedException("Insufficient money on your account!");
        }
        card.setAmount(card.getAmount() - seatPrice);
        paymentRepository.save(card);
        ReserveDto reserveJson = new ReserveDto();
        reserveJson.setReservationId("34175676");
        reserveJson.setSuccess("true");
        return reserveJson;
    }
}