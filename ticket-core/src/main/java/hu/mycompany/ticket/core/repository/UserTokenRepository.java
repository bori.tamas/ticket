package hu.mycompany.ticket.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.mycompany.ticket.core.domain.UserToken;

public interface UserTokenRepository extends JpaRepository<UserToken, Integer> {

    /**
     * Finds UserToken entity with the given user id.
     */
    UserToken findByUserId(Integer userId);
}
