package hu.mycompany.ticket.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Entity to represent one record from the collection of user bank card in a separate
 * table.
 */
@Entity
@Audited
@EntityListeners(AuditingEntityListener.class)
@Table(name = "userbankcard")
@DynamicUpdate
@DynamicInsert
public class UserBankCard {

    @Id
    @Column(name = "userbankcardid", updatable = false)
    private Integer userbankcardId;

    @Column(name = "userid", updatable = false)
    private Integer userId;
    
    @Column(name = "cardid", updatable = false)
    private String cardId;

    @Column(name = "cardnumber")
    private String cardnumber;
    
    @Column(name = "cvc")
    private int cvc;
    
    @Column(name = "name")
    private String name;

    @Column(name = "amount")
    private int amount;
    
    @Column(name = "currency")
    private String currency;

    public Integer getUserbankcardId() {
        return userbankcardId;
    }

    public void setUserbankcardId(Integer userbankcardId) {
        this.userbankcardId = userbankcardId;
    }
    
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
