package hu.mycompany.ticket.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.mycompany.ticket.core.domain.UserDevice;

public interface UserDeviceRepository extends JpaRepository<UserDevice, Integer> {

    /**
     * Finds UserDevice entities with the given user id.
     */
    List<UserDevice> findByUserId(Integer userId);
}
