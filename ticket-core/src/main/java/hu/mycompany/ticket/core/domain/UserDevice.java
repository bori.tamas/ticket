package hu.mycompany.ticket.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Entity to represent one record from the collection of user device in a separate
 * table.
 */
@Entity
@Audited
@EntityListeners(AuditingEntityListener.class)
@Table(name = "userdevice")
public class UserDevice {

    @Id
    @Column(name = "userdeviceid", updatable = false)
    private int userDeviceId;
    
    @Column(name = "userid", updatable = false)
    private int userId;
    
    @Column(name = "devicehash", updatable = false)
    private String deviceHash;

    public int getUserDeviceId() {
        return userDeviceId;
    }

    public void setUserDeviceId(int userDeviceId) {
        this.userDeviceId = userDeviceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceHash() {
        return deviceHash;
    }

    public void setDeviceHash(String deviceHash) {
        this.deviceHash = deviceHash;
    }
}
