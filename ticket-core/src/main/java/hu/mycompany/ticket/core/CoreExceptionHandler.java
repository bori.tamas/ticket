package hu.mycompany.ticket.core;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import hu.mycompany.ticket.api.exception.GlobalExceptionHandler;

@RestControllerAdvice
public class CoreExceptionHandler extends GlobalExceptionHandler {

}
