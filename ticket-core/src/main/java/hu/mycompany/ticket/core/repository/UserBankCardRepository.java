package hu.mycompany.ticket.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hu.mycompany.ticket.core.domain.UserBankCard;

@Repository
public interface UserBankCardRepository extends JpaRepository<UserBankCard, Integer> {

    /**
     * Finds UserBankCard entity with the given user id.
     */
    UserBankCard findByUserId(Integer userId);
}
