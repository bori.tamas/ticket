package hu.mycompany.ticket.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.mycompany.ticket.core.domain.Users;

public interface UsersRepository extends JpaRepository<Users, Integer> {

    /**
     * Finds Users entity with the given user id.
     */
    Users findByUserId(Integer userId);

}
