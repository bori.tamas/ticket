package hu.mycompany.ticket.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.mycompany.ticket.api.dto.ReserveDto;
import hu.mycompany.ticket.api.handlers.PaymentHandlerIF;
import hu.mycompany.ticket.core.service.PaymentService;

@RestController
@RequestMapping("/payment")
public class PaymentController implements PaymentHandlerIF {

    @Autowired
    private PaymentService paymentService;
    
    @Override
    public ReserveDto pay(int eventId, int userId, String cardNumber, int seatPrice) {
        return paymentService.pay(eventId, userId, cardNumber, seatPrice);
    }
}