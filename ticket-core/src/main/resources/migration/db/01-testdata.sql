/* Users rekordok */
INSERT INTO Users (userid,name,email) VALUES (1000,'Teszt Aladár','teszt.aladar@otpmobil.hu');
INSERT INTO Users (userid,name,email) VALUES (2000,'Teszt Benedek','teszt.benedek@otpmobil.hu');
INSERT INTO Users (userid,name,email) VALUES (3000,'Teszt Cecília','teszt.cecilia@otpmobil.hu');

/* UserDevice rekordok */
INSERT INTO UserDevice (userdeviceid,userid,devicehash) VALUES (1000,1000,'F67C2BCBFCFA30FCCB36F72DCA22A817');
INSERT INTO UserDevice (userdeviceid,userid,devicehash) VALUES (2000,1000,'0F1674BD19D3BBDD4C39E14734FFB876');
INSERT INTO UserDevice (userdeviceid,userid,devicehash) VALUES (3000,1000,'3AE5E9658FBD7D4048BD40820B7D227D');
INSERT INTO UserDevice (userdeviceid,userid,devicehash) VALUES (4000,2000,'FADDFEA562F3C914DCC81956682DB0FC');
INSERT INTO UserDevice (userdeviceid,userid,devicehash) VALUES (5000,3000,'E68560872BDB2DF2FFE7ADC091755378');

/* UserToken rekordok */
INSERT INTO UserToken (usertokenid,userid,token) VALUES (1000,1000,'dGVzenQuYWxhZGFyQG90cG1vYmlsLmNvbSYxMDAwJkY2N0MyQkNCRkNGQTMwRkNDQjM2RjcyRENBMjJBODE3');
INSERT INTO UserToken (usertokenid,userid,token) VALUES (2000,2000,'dGVzenQuYmVuZWRla0BvdHBtb2JpbC5jb20mMjAwMCZGQURERkVBNTYyRjNDOTE0RENDODE5NTY2ODJEQjBGQw==');
INSERT INTO UserToken (usertokenid,userid,token) VALUES (3000,3000,'dGVzenQuY2VjaWxpYUBvdHBtb2JpbC5jb20mMzAwMCZFNjg1NjA4NzJCREIyREYyRkZFN0FEQzA5MTc1NTM3OA==');
INSERT INTO UserToken (usertokenid,userid,token) VALUES (4000,1000,'dGVzenQuYWxhZGFyQG90cG1vYmlsLmNvbSYxMDAwJjBGMTY3NEJEMTlEM0JCREQ0QzM5RTE0NzM0RkZCODc2');
INSERT INTO UserToken (usertokenid,userid,token) VALUES (5000,1000,'dGVzenQuYWxhZGFyQG90cG1vYmlsLmNvbSYxMDAwJjNBRTVFOTY1OEZCRDdENDA0OEJENDA4MjBCN0QyMjdE');

/* UserBankCard rekordok */
INSERT INTO UserBankCard (userbankcardid,userid,cardid,cardnumber,cvc,name,amount,currency) VALUES (1000,1000,'C001','5299706965433676','123','Teszt Aladár',1000,'HUF');
INSERT INTO UserBankCard (userbankcardid,userid,cardid,cardnumber,cvc,name,amount,currency) VALUES (2000,2000,'C002','5390508354245119','456','Teszt Benedek',2000,'HUF');
INSERT INTO UserBankCard (userbankcardid,userid,cardid,cardnumber,cvc,name,amount,currency) VALUES (3000,3000,'C003','4929088924014470','789','Teszt Cecília',3000,'HUF');